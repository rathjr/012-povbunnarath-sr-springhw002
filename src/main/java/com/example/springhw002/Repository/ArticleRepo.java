package com.example.springhw002.Repository;

import com.example.springhw002.Model.DTO.ArticleDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public class ArticleRepo {

    private ArrayList<ArticleDTO> article;
    public ArticleRepo (){
        article=new ArrayList<>();
        for(int i=1;i<=3;i++){
            article.add(new ArticleDTO(i,"Anonymous"+i,"Anonymous is a decentralized international activist"+i,"hacker-with-laptop_23-2147985341.jpg"));
        }
    }
    public ArrayList<ArticleDTO> getAlllist(){
        return article;
    }

    public ArticleDTO getView(int id){
        for(ArticleDTO articleDTO:article){
            if(articleDTO.getId()==id){
                return articleDTO;
            }
        }
        return null;
    }
    public void setIDauto(ArticleDTO articleDTO){
        int count=0;
        count=article.size();
        count++;
        articleDTO.setId(count);
    }
    public boolean Save(ArticleDTO articleDTO){
        return article.add(articleDTO);
    }
    public boolean Update(ArticleDTO articleDTO){
        System.out.println("test"+articleDTO.getId());
        for(int i=0;i<article.size();i++){
            if(articleDTO.getId()==article.get(i).getId()){
                article.get(i).setId(articleDTO.getId());
                article.get(i).setTitle(articleDTO.getTitle());
                article.get(i).setDescription(articleDTO.getDescription());
                article.get(i).setThumbnail(articleDTO.getThumbnail());
                return true;
            }
        }
//        return false;
//        article.stream().filter(e->e.getId()==articleDTO.getId())
//                .forEach(e->{e.setTitle(articleDTO.getTitle());
//                    System.out.println("id"+ e.getId());
//                    System.out.println("id2"+ articleDTO.getId());
//                });
//    return true;
        return false;
    }

    public boolean Delete(int id){
        for(ArticleDTO art:article){
            if(art.getId()==id) {
                article.remove(art);
                return true;
            }
        }
        return false;
    }
}
