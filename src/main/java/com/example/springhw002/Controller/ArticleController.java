package com.example.springhw002.Controller;

import com.example.springhw002.Model.DTO.ArticleDTO;
import com.example.springhw002.Model.Fileupload;
import com.example.springhw002.Service.ServiceImp.ArticleIservicemplement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;

@Controller
public class ArticleController {
    @Autowired
    ArticleIservicemplement articleIservicemplement;
    @Autowired
    Fileupload fileupload;

    public ArticleController(ArticleIservicemplement articleIservicemplement) {
        this.articleIservicemplement = articleIservicemplement;
    }

    @RequestMapping({"/", "index"})
    public String Homepage(Model model) {
        ArrayList<ArticleDTO> article = articleIservicemplement.getAlllist();
        model.addAttribute("article", article);
        return "index";
    }


    @GetMapping("/create")
    public String Createarticle() {
        return "create";
    }

    @PostMapping("/create")
    public String getCreate(@RequestParam("file") MultipartFile file, @ModelAttribute ArticleDTO articleDTO) {
        articleIservicemplement.setIDauto(articleDTO);
        String thumbnail = fileupload.upload(file);
        articleDTO.setThumbnail(thumbnail);
        ArrayList<ArticleDTO> article = articleIservicemplement.getAlllist();
        if (articleIservicemplement.Save(articleDTO))
            System.out.println("save success !");
        else System.out.println("Save faile !");
        System.out.println(articleDTO);
        return "redirect:/index";
    }

    @GetMapping("/view/{id}")
    public String Viewpage(Model model, @PathVariable("id") Integer id,ArticleDTO articleDTO) {
        System.out.println("id" + id);
        articleDTO = articleIservicemplement.getView(id);
        model.addAttribute("article", articleDTO);
        return "view";
    }

    @GetMapping("/update/{id}")
    public String Update(@PathVariable("id") Integer id, Model model,ArticleDTO articleDTO) {
        ArrayList<ArticleDTO> article = articleIservicemplement.getAlllist();
        articleDTO = articleIservicemplement.getView(id);
        System.out.println("Get" + articleDTO);
        model.addAttribute("updatelist", articleDTO);
        return "update";
    }

    @PostMapping("/update")
    public String getUpdate(@RequestParam("file")MultipartFile file,@ModelAttribute ArticleDTO articleDTO) {
        ArrayList<ArticleDTO> article = articleIservicemplement.getAlllist();
        if(articleIservicemplement.Update(articleDTO)){
//           try{
//               String filename=file.getOriginalFilename();
//               Path path= Paths.get("src/main/resources/files/"+filename);
//               Files.copy(file.getInputStream(),path, StandardCopyOption.REPLACE_EXISTING);
//           }catch (Exception e){
//               System.out.println(e.getMessage());
//           }
           String thumbnail = fileupload.upload(file);
            System.out.println("thumnul"+thumbnail);
           if(thumbnail.equals("")) {
               System.out.println("no");;

           }else{
               article.get(articleDTO.getId() - 1).setThumbnail(thumbnail);
               System.out.println("yes");
           }

       }else {
           System.out.println("update failed !");
       }
        System.out.println(article);
        return "redirect:/index";
    }

    @GetMapping("/delete/{id}")
    public String Deletepage(Model model, @PathVariable("id") Integer id,ArticleDTO articleDTO){
        if(articleIservicemplement.Delete(id)){
            System.out.println("delete success !");
        }else
            System.out.println("delete failed !");
        model.addAttribute("article",articleDTO);
        return "redirect:/index";
    }
}
