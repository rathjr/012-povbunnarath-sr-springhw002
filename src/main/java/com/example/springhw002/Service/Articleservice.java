package com.example.springhw002.Service;

import com.example.springhw002.Model.DTO.ArticleDTO;

import java.util.ArrayList;

public interface Articleservice {
    public ArrayList<ArticleDTO> getAlllist();
    public ArticleDTO getView(int id);
    public void setIDauto(ArticleDTO articleDTO);
    public boolean Save(ArticleDTO articleDTO);
    public boolean Update(ArticleDTO articleDTO);
    public boolean Delete(int id);
}
