package com.example.springhw002;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springhw002Application {

    public static void main(String[] args) {
        SpringApplication.run(Springhw002Application.class, args);
    }

}
