package com.example.springhw002.Service.ServiceImp;

import com.example.springhw002.Model.DTO.ArticleDTO;
import com.example.springhw002.Repository.ArticleRepo;
import com.example.springhw002.Service.Articleservice;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
@Service
public class ArticleIservicemplement implements Articleservice {
    @Autowired
    ArticleRepo articleRepo;

    public ArticleIservicemplement(ArticleRepo articleRepo){
        this.articleRepo=articleRepo;
    }


    @Override
    public ArrayList<ArticleDTO> getAlllist() {
        return articleRepo.getAlllist();
    }

    @Override
    public ArticleDTO getView(int id) {
        return articleRepo.getView(id);
    }

    @Override
    public void setIDauto(ArticleDTO articleDTO) {
        articleRepo.setIDauto(articleDTO);
    }

    @Override
    public boolean Save(ArticleDTO articleDTO) {
        return articleRepo.Save(articleDTO);
    }

    @Override
    public boolean Update(ArticleDTO articleDTO) {
        return articleRepo.Update(articleDTO);
    }

    @Override
    public boolean Delete(int id) {
        return articleRepo.Delete(id);
    }
}
